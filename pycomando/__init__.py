

from __future__ import absolute_import

__docformat__ = 'restructuredtext'
__version__ = "0.2.0"

from pycomando.pycomando import (
    Comando,
)

__all__ = ('Comando')